package cn.wxqsearch.enemy;

import java.awt.image.BufferedImage;
import cn.wxqsearch.tools.Images;

public class BigAirplane extends Enemy {
	public BigAirplane() {
		super(Images.bigImg[0].getWidth(), Images.bigImg[0].getHeight(), 0);
		this.score=2;
	}

	@Override
	public BufferedImage getImage() {
		if (status > Images.bigImg.length-1) {
			return null;
		}
		return Images.bigImg[status];
	}
	
}
