package cn.wxqsearch.enemy;

import java.awt.image.BufferedImage;
import cn.wxqsearch.tools.Images;

public class Bee extends Enemy {
	public Bee() {
		super(Images.beeImg[0].getWidth(), Images.beeImg[0].getHeight(), 1);
		this.score=3;
	}

	@Override
	public BufferedImage getImage() {
		if (status > Images.beeImg.length-1) {
			return null;
		}
		return Images.beeImg[status];
	}
	
}
