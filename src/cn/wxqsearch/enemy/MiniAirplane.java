package cn.wxqsearch.enemy;

import java.awt.image.BufferedImage;
import cn.wxqsearch.tools.Images;

public class MiniAirplane extends Enemy {
	public MiniAirplane() {
		super(Images.miniImg[0].getWidth(), Images.miniImg[0].getHeight(), 0);
		this.score=1;
	}

	@Override
	public BufferedImage getImage() {
		if (status > Images.miniImg.length-1) {
			return null;
		}
		return Images.miniImg[status];
	}
	
}
