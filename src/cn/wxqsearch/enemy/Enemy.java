package cn.wxqsearch.enemy;

import java.awt.Image;
import java.util.Random;

import cn.wxqsearch.World;
import cn.wxqsearch.common.Flying;
import cn.wxqsearch.tools.Config;

public class Enemy extends Flying {
	//生命
	public final static int REWARD_LIFE = 0;
	//未设置
	public final static int REWARD_NULL = 1;
	//火力值
	public final static int REWARD_FIRE_2 = 2;
	public final static int REWARD_FIRE_3 = 3;
	public final static int REWARD_FIRE_4 = 4;
	public int score;
	public Enemy(int width, int height, int x_speed) {
		super(new Random().nextInt(Config.WORLD_WIDTH - width), -height, width, height, x_speed, 1);
		this.status=0;
	}

	@Override
	public Image getImage() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void allMove() {
		super.allMove();
		if (x>=Config.WORLD_WIDTH - this.height) {
			x_speed=-x_speed;
		}
	}
	
	/**
	 * 处理碰撞
	 * @param other
	 * @return
	 */
	public boolean is_against(Flying other) {
		//敌人左上、右上、左上、左下
		int x1 = this.x;
		int x2 = this.x + this.width;
		int y1 = this.y;
		int y3 = this.y + this.height;
		//英雄或子弹 左上、右上、左上、左下
		int xx1 = other.x;
		int xx2 = other.x+other.width;
		int yy1 = other.y;
		
		//纵坐标只要小于敌人左下坐标，就算碰撞。
		if (yy1<y3) {
			//英雄或子弹左上碰撞
			if (xx1>x1 && xx1<x2) {
				return true;
			}
			//英雄或子弹右上碰撞
			if (xx2>x1 && xx2<x2) {
				return true;
			}
		}
		
		return false;
	}
	/**
	 * 随机获取奖励类型
	 */
	public int getRewardType() {
		return new Random().nextInt(4);
	}
	public int getScore() {
		return this.score;
	}
}
