package cn.wxqsearch.common;

import java.awt.Image;
import java.awt.image.BufferedImage;

public abstract class Flying {
	public final static int STATUS_INIT=0;
	public final static int STATUS_DEL=4;
	public int x=0;
	public int y=0;
	public int width;
	public int height;
	public int x_speed;
	public int y_speed;
	public int status;
	public abstract Image getImage();
	public Flying() {
		
	}
	
	public Flying(int x, int y, int width, int height, int x_speed, int y_speed) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.x_speed = x_speed;
		this.y_speed = y_speed;
		this.status=0;
	}
	/**
	 * 移动物体移动1次 不判断越界
	 */
	public void allMove() {
		this.y+=y_speed;
		this.x+=x_speed;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getStatus() {
		return this.status;
	}
	/**
	 * 敌人死亡
	 */
	public void goDie() {
		status++;
	}
	
}
