package cn.wxqsearch.common;

import java.awt.Image;

import cn.wxqsearch.tools.Images;

public class Sky extends Flying {
	private int x;
	private int y;
	private int y1;
	private int speed = 2;
	private int height;
	private int width;
	//初始化天空坐标
	public Sky() {
		width=400;
		height=700;
		x=0;
		y=0;
		y1=-height;
	}
	public Image getImage() {
		return Images.skyImg;
	}
	@Override
	public void allMove() {
		y+=speed;
		y1+=speed;
		if (y >= height) {
			y = -height;
		}
		if (y1 >= height) {
			y1=-height;
		}
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public int getY1() {
		return y1;
	}
	
}
