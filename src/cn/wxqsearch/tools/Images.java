package cn.wxqsearch.tools;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * 加载所有图片组件
 * @author 阿P
 *
 */
public class Images {
	public static BufferedImage skyImg;
	public static BufferedImage[] heroImg = new BufferedImage[2];
	public static BufferedImage[] bigImg = new BufferedImage[5];
	public static BufferedImage[] miniImg = new BufferedImage[5];
	public static BufferedImage[] beeImg = new BufferedImage[5];
	public static BufferedImage bulletImg;
	public static BufferedImage startImg;
	public static BufferedImage pauseImg;
	public static BufferedImage overImg;
	

	static {
		try {
			//加载天空图片
			skyImg = ImageIO.read(Images.class.getResource("/images/sky.png"));
			bulletImg = ImageIO.read(Images.class.getResource("/images/bullet.png"));
			heroImg[0] = ImageIO.read(Images.class.getResource("/images/hero0.png"));
			heroImg[1] = ImageIO.read(Images.class.getResource("/images/hero1.png"));
			startImg = ImageIO.read(Images.class.getResource("/images/start.png"));
			pauseImg = ImageIO.read(Images.class.getResource("/images/pause.png"));
			overImg = ImageIO.read(Images.class.getResource("/images/over.png"));
			for (int i = 0; i < bigImg.length; i++) {
				bigImg[i] = ImageIO.read(Images.class.getResource("/images/big"+i+".png"));
				miniImg[i] = ImageIO.read(Images.class.getResource("/images/mini"+i+".png"));
				beeImg[i] = ImageIO.read(Images.class.getResource("/images/bee"+i+".png"));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
