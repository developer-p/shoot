package cn.wxqsearch.tools;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

public class Config {
	public static Map<String, String> config = new HashMap<String, String>();
	//游戏运行状态
	public final static int STATUS_START=0;
	public final static int STATUS_RUNNING=1;
	public final static int STATUS_PAUSE=2;
	public final static int STATUS_OVER=3;
	//世界大小
	public final static int WORLD_WIDTH=400;
	public final static int WORLD_HEIGHT=700;
	static {
		try {
			Properties prop = new Properties();
			InputStream ins = Config.class.getClassLoader().getResourceAsStream("config.txt");
			prop.load(ins);
			config.put("make_bullet_speed", prop.getProperty("make_bullet_speed"));//生成子弹速度
			config.put("make_enemy_speed", prop.getProperty("make_enemy_speed"));//生成敌人速度
			config.put("hero_life", prop.getProperty("hero_life"));//敌人初始生命值
			config.put("hero_fire", prop.getProperty("hero_fire"));//英雄机初始生命值
			config.put("enemy_probability", prop.getProperty("enemy_probability"));//敌人出现概率（尚未实现）
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 返回敌人概率 （尚未实现）
	 * @return
	 */
	public static String getRandomEnemy() {
		int probability = 0;//总概率
		Map<String, Integer> pro = new HashMap();
		try {
			for (String s : config.get("enemy_probability").split(";")) {
				String[] arr = s.split(":");
				probability+=Integer.parseInt(arr[1]);
				pro.put(arr[0], Integer.parseInt(arr[1]));
			}
			int i = new Random().nextInt(probability);
			for (String p:pro.keySet()) {
				if (true) {
					
				}
			}
			return "cn.wxqsearch.enemy."+"";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
}
