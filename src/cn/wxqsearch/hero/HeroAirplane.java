package cn.wxqsearch.hero;

import java.awt.Image;

import cn.wxqsearch.common.Flying;
import cn.wxqsearch.enemy.Enemy;
import cn.wxqsearch.tools.Config;
import cn.wxqsearch.tools.Images;

public class HeroAirplane extends Flying {
	private int fire;
	private int life;
	//顺序切换英雄机图片。
	private int index = 0;
	//得分
	private int score;
	public HeroAirplane() {
		life=Integer.parseInt(Config.config.get("hero_life"));
		x=140;
		y=400;
		height=Images.heroImg[0].getHeight();;
		width=Images.heroImg[0].getWidth();;
		fire=Integer.parseInt(Config.config.get("hero_fire"));;//火力倍数
		score=0;
	}
	/**
	 * 英雄机死亡
	 */
	public void goDie() {
		//重置位置(鼠标控制，没效果；方便后期改成键盘控制)
		x=140;
		y=400;
		//生命减一
		if (--life <=0 )
			life=0;
		//火力值清空
		fire=1;
	}
	/*
	 * 获取对应英雄机图片
	 */
	public Image getImage() {
		return Images.heroImg[++index%2];
	}
	
	//英雄机移动
	public void move(int x, int y) {
		this.x=x-width/2;
		this.y=y-height/2;
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public int getHeight() {
		return height;
	}
	public int getWidth() {
		return width;
	}
	
	public int getFire() {
		return fire;
	}
	public void setLife(int life) {
		this.life=life;
	}
	public int getLife() {
		return life;
	}
	
	/**
	 * 加分 加奖励
	 * @param e
	 */
	public void addReward(Enemy e) {
		switch (e.getRewardType()) {
		case Enemy.REWARD_LIFE:
			life++;
			break;
		case Enemy.REWARD_FIRE_2:
			fire=2;
			break;
		case Enemy.REWARD_FIRE_3:
			fire=3;
			break;
		case Enemy.REWARD_FIRE_4:
			fire=4;
			break;
		}
		score+=e.getScore();
	}
	public int getScore() {
		return this.score;
	}
}
