package cn.wxqsearch.hero;

import java.awt.Image;
import java.awt.image.BufferedImage;

import cn.wxqsearch.common.Flying;
import cn.wxqsearch.tools.Images;

public class Bullet extends Flying {
	
	/**
	 * @param hero 英雄机
	 * @param fire_index 火力点
	 */
	public Bullet(HeroAirplane hero, int fire_index) {
		//火力值等分。例如：单倍火力2等分 2倍火力四等分 3倍火力6等分
		int avg = hero.getWidth()/(hero.getFire()*2);
		this.x=hero.getX() + (fire_index*2-1) * avg;
		this.y=hero.getY() - hero.getHeight()/2;
		y_speed=-1;
	}
	
	public BufferedImage getImage() {
		if (status==0) {
			return Images.bulletImg;
		}
		return null;
	}

}
