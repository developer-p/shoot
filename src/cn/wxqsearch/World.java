package cn.wxqsearch;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import cn.wxqsearch.common.Flying;
import cn.wxqsearch.common.Sky;
import cn.wxqsearch.enemy.Bee;
import cn.wxqsearch.enemy.BigAirplane;
import cn.wxqsearch.enemy.Enemy;
import cn.wxqsearch.enemy.MiniAirplane;
import cn.wxqsearch.hero.Bullet;
import cn.wxqsearch.hero.HeroAirplane;
import cn.wxqsearch.tools.Config;
import cn.wxqsearch.tools.Images;

/**
 * 画板主体
 * @author 阿P
 *
 */
public class World extends JPanel {
	private final static long serialVersionUID = 1L;

	//游戏状态
	private int status;
	private Sky sky;
	private HeroAirplane hero;
	private List<Enemy> enemys;
	private List<Bullet> bulletes;
	
	//无意义索引
	private int enemy_index;//控制敌人产生时间的索引
	private int bullet_index;//控制子弹产生时间的索引
	private int out_index;//控制越界处理的索引

	public World() {
		init();
	}

	/**
	 * 初始化全部操作
	 */
	public void init() {
		sky = new Sky();
		hero = new HeroAirplane();
		enemys = new ArrayList<Enemy>();
		bulletes = new ArrayList<Bullet>();
		status=Config.STATUS_START;
		enemy_index=0;
		out_index=0;
		bullet_index=0;
	}
	/**
	 * 生成随机敌人
	 */
	private Enemy getRandomEnemy() {
		int tmp = new Random().nextInt(500);
		if (tmp > 300) {
			return new MiniAirplane();
		} else if (tmp > 50) {
			return new BigAirplane();
		} else {
			return new Bee();
		}
	}
	private void mouseAction() {
		//鼠标侦听器
		MouseAdapter l = new MouseAdapter() {
			//鼠标移动事件
			public void mouseMoved(MouseEvent e){
				if (status == Config.STATUS_RUNNING) {
					hero.move(e.getX(), e.getY());
				}

			}
			//鼠标点击
			@Override
			public void mousePressed(MouseEvent e) {
				//双击启动或暂停 双击重新开始
				if (e.getClickCount()>=2) {
					switch (status) {
					case Config.STATUS_START:
						status = Config.STATUS_RUNNING;
						break;
					case Config.STATUS_RUNNING:
						status=Config.STATUS_PAUSE;
						break;
					case Config.STATUS_PAUSE:
						status=Config.STATUS_RUNNING;
						break;
					case Config.STATUS_OVER:
						status = Config.STATUS_START;
						//重新开始
						init();
						break;
					}
				}
			}
		};
		this.addMouseListener(l);
		this.addMouseMotionListener(l);
	}
	/**
	 * 生成敌人 、子弹
	 */
	private void makeFlying() {
		//敌人生成速度
		int make_bullet_speed = Integer.parseInt(Config.config.get("make_bullet_speed"));
		if (bullet_index++%make_bullet_speed==0) {
			enemys.add(getRandomEnemy());
		}
		//子弹生成速度
		int make_enemy_speed = Integer.parseInt(Config.config.get("make_enemy_speed"));
		if (enemy_index++%make_enemy_speed==0) {
			for (int i = 1; i <= hero.getFire(); i++) {
				bulletes.add(new Bullet(hero, i));
			}
		}
	}
	/*
	 * 所有物体移动
	 */
	private void allMove() {
		//10毫秒天空移动
		sky.allMove();
		//10毫秒敌人移动
		for (Enemy e : enemys) {
			e.allMove();
		}
		//10毫秒子弹移动
		for (Bullet b : bulletes) {
			b.allMove();
		}

	}

	/**
	 * 删除越界
	 */
	private void outOfBounds() {
		if (out_index++%40==0) {
			//敌人越界
			if (bulletes.size()>0) {
				//子弹越界
				for (int i =0; i<bulletes.size(); i++) {
					if (bulletes.get(i).getY() <= bulletes.get(i).getHeight()) {
						bulletes.remove(i);
					}
				}
			}
			if (enemys.size()>0) {
				for (int i =0; i<enemys.size(); i++) {
					if (enemys.get(i).getY() >= Config.WORLD_HEIGHT) {
						enemys.remove(i);
					}
				}
			}
		}
	}
	/**
	 * 处理碰撞
	 */
	private void hit() {

		for (Enemy e : enemys) {
			if (e.getStatus() != Flying.STATUS_INIT) {
				//不是活着的状态，状态继续新增，直到删除。
				e.goDie();
				continue;
			}
			//子弹、敌人碰撞
			for (Flying b : bulletes) {
				if (b.getStatus() != Flying.STATUS_INIT) {
					continue;
				}
				if (e.is_against(b)) {
					b.goDie();
					e.goDie();
					//加分 或者 奖励
					hero.addReward(e);
				}
			}
			//英雄机、敌人碰撞
			if (e.is_against(hero)) {
				e.goDie();
				hero.goDie();
				if (hero.getLife()<=0) {
					gameOver();
					return;
				}
			}
		}

	}

	/**
	 * 游戏结束
	 */
	private void gameOver() {
		this.status=Config.STATUS_OVER;
	}
	private void myTimer() {
		//自动产生飞机、子弹
		while (true) {
			try {
				if (status == Config.STATUS_RUNNING) {
					//400毫秒生成1个敌人 1次子弹
					makeFlying();
					//10毫秒移动物体移动1次
					allMove();
					//400毫秒处理一次越界
					outOfBounds();
					//10毫秒处理一次碰撞
					hit();
				}
				//10毫秒重构一次页面
				repaint();
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			} finally {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	/**
	 * 开始程序
	 */
	private void start() {
		mouseAction();
		myTimer();

		//移动子弹
		//移动敌人
		//移动英雄机
		//处理各种碰撞
		//
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		//画天空
		g.drawImage(sky.getImage(), sky.getX(), sky.getY(), null);
		g.drawImage(sky.getImage(), sky.getX(), sky.getY1(), null);
		//画英雄机
		g.drawImage(hero.getImage(), hero.getX(), hero.getY(), null);
		//画敌人
		for (Enemy e : enemys) {
			g.drawImage(e.getImage(), e.getX(), e.getY(), null);
		}
		//子弹
		for (Bullet b : bulletes) {
			g.drawImage(b.getImage(), b.getX(), b.getY(), null);
		}
		//画分数
		g.drawString("SCORE:"+hero.getScore(), 20, 40);
		//画生命
		g.drawString("LIFE:"+hero.getLife(), 20, 60);
		switch (status) {
		case Config.STATUS_START:
			g.drawImage(Images.startImg, 0, 0, null);
			break;
		case Config.STATUS_PAUSE:
			g.drawImage(Images.pauseImg, 0, 0, null);
			break;
		case Config.STATUS_RUNNING:
			break;
		case Config.STATUS_OVER:
			g.drawImage(Images.overImg, 0, 0, null);
			break;
		}
	}

	public static void main(String[] args) {
		try {
			JFrame frame = new JFrame("Airplane Shoot Game");
			World world = new World();
			//制作画板、添加画板
			frame.add(world);
			frame.setSize(Config.WORLD_WIDTH, Config.WORLD_HEIGHT);
			frame.setAlwaysOnTop(true);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setIconImage(new ImageIcon("images/icon.jpg").getImage());
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);//设置窗口可见；调用paint
			world.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
